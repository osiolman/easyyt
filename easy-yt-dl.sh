#!/bin/bash

cd /home

function wybor
{
echo "                     Wpisz adres do pobrania:"

read adres
echo ""
echo "                     Chcesz pobrac piosenke czy film?"
echo ""
echo "                     1. muzyka (mp3) (ffmpg)"
echo "                     2. film (mp4) (ffmpg)"
echo "                     3. inny format wideo (ffmpg)"
echo "                     4. inny format audio (ffmpg)"
echo "                     5. lista formatow kompatybilnych"
read opc

if [ "$opc" = 2 ]
then
echo "                     Prosze cierpliwie czekac"
youtube-dl --recode-video mp4 "$adres"
echo "                     Plik został pobrany "
elif [ "$opc" = 1 ]
then
echo "                     Prosze cierpliwie czekac"
youtube-dl -x --audio-format mp3 "$adres"
echo "                     Plik został pobrany "
elif [ "$opc" = 3 ]
then
echo "                     Podaj format"
read format
youtube-dl --recode-video "$format" "$adres"
elif [ "$opc" = 4 ]
then
echo "                     Podaj format"
read format
youtube-dl -x --audio-format "$format" "$adres"
echo "                     Plik został pobrany "
elif [ "$opc" = 5 ]
then
youtube-dl -F "$adres"
echo "Jaki format wybierasz?"
read opcja
youtube-dl -f "$opcja" "$adres"
else
echo "                     Zly wybor"

fi


}

function wideo
{
echo "                     Wpisz nazwe filmu:"
echo "                     (autor_tytul)"
read nazwa
youtube-dl --recode-video mp4 "ytsearch:"$nazwa""
echo "                     Plik został pobrany "

}

function audio
{
echo "                     Wpisz nazwe piosenki:"
echo "                     (autor_tytul)"
read nazwa
youtube-dl -x --audio-format mp3 "ytsearch:"$nazwa""
echo "                     Plik został pobrany "

}


clear


echo -e "\e[31m    _______________________________________________________________ \e[0m"
echo -e "\e[31m   |                                                               | \e[0m"
echo -e "\e[31m   |                           |||||                               |   \e[0m"
echo -e "\e[31m   |                           ||||||||                            |   \e[0m"
echo -e "\e[31m   |                           |||||||||||                         |   \e[0m"
echo -e "\e[31m   |                           |||||||||||||                       |   \e[0m"
echo -e "\e[31m   |                           ||||||||||||||                      |   \e[0m"
echo -e "\e[31m   |                           |||||||||||||                       |   \e[0m"
echo -e "\e[31m   |                           |||||||||||                         |   \e[0m"
echo -e "\e[31m   |                           ||||||||                            |   \e[0m"
echo -e "\e[31m   |                           |||||                               |   \e[0m"
echo -e "\e[31m   |_______________________________________________________________| \e[0m"

echo ""
echo "                     Witaj w programie Youtube-dl"


echo ""
echo "                     1. Wyszukaj i pobierz wideo  "
echo "                     2. Wyszukaj i pobierz audio "
echo "                     3. Wpisz adres do pobrania"

read odp

if [ "$odp" = 1 ]
then
wideo
elif [ "$odp" = 2 ]
then
audio

elif [ "$odp" = 3 ]
then
wybor
else
echo "                     Zly wybor"

fi



#Nakladka zrobiona przez osiolmana
#Mozna modyfikowac
